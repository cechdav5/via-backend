const express = require('express')
const swaggerUi = require("swagger-ui-express")
const authRouter = require('./routes/auth')
const planRouter = require('./routes/plans')
const mongoose = require('mongoose')
const swaggerFile = require('./swagger.json')
var cors = require('cors')

require('dotenv').config()

const app = express()
app.use(express.json())

app.use(cors())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, authorization");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE');
    next();
});

app.use('/auth', authRouter)
app.use('/plans', planRouter)

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile))

const {
    DB_HOST,
    MONGODB_DATABASE,
    MONGODB_LOCAL_PORT,
    NODE_DOCKER_PORT
} = process.env;

mongoose.connect('mongodb://localhost:27017/via');

setTimeout(() => (console.log("Mongoose ready state ", mongoose.connection.readyState)), 5000);

app.listen(5050, () => {
    console.log("Server started on port", 5050)
})