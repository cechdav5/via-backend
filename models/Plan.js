const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const planSchema = new Schema({
    user: String,
    name:  String,
    description: String,
    position: [Number, Number],
    startDate: Number,
    endDate: Number,
    attendees: Number
});

module.exports = mongoose.model("Plan", planSchema)
