# VIA - Event Planner Backend

Projekt je webovým serverem pro [frontend]([https://gitlab.fel.cvut.cz/cechdav5/via-frontend](https://gitlab.fel.cvut.cz/cechdav5/via-frontend)) semestrálního projektu v rámci předmětu VIA. Podrobnější informace o API dostupné [zde](http://207.154.251.145:5050/api-docs/)

## Dostupné skripty

V adresáři projektu lze spustit následující příkazy:

### `npm start`

Spustí node server na adrese [http://localhost](http://localhost) s portem, který je specifikovaný v konfiguračním souboru .env.


### `npm run dev`

Spustí node server ve vývojovém režimu pomocí nástroje nodemon na adrese [http://localhost](http://localhost) s portem, který je specifikovaný v konfiguračním souboru .env..

Stránka se načte znovu po změně zdrojového kódu.