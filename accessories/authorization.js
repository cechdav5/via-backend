const jwt = require("jsonwebtoken");

function authorization(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) {
        return res.sendStatus(401)
    }
    jwt.verify(token, "8b8512af8834dfb75daada05bda919d8e24336887356b88589b4a3c6de9c202108b0d4248d15afe48e41085cf040889065380a9301977f9f875136027fc2fb11", (err, user) => {
        if (err) return res.sendStatus(403)
        req.user = user
        next()
    })
}

module.exports = authorization
