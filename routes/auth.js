const express = require('express')
const router = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("../models/User")

router.post("/login", async (req, res) => {
    User.findOne({email: req.body.email}).lean().exec(async function (err, foundUser) {
        if (err) {
            res.status(500).send()
        } else {
            try {
                if (foundUser && await bcrypt.compare(req.body.password, foundUser.password)) {
                    const accessToken = jwt.sign(foundUser, "8b8512af8834dfb75daada05bda919d8e24336887356b88589b4a3c6de9c202108b0d4248d15afe48e41085cf040889065380a9301977f9f875136027fc2fb11")
                    res.json({accessToken: accessToken, username: foundUser.username})
                } else {
                    res.status(401).send()
                }
            } catch (err) {
                res.status(500).send()
            }
        }
    })
})

router.post("/register", async (req, res) => {
    User.countDocuments({email: req.body.email}, async function (err, count) {
        if (err) {
            res.status(500).send()
        } else if (count > 0) {
            res.status(400).send()
        } else {
            try {
                const hashedPassword = await bcrypt.hash(req.body.password, 10)
                let newUser = new User();
                newUser.email = req.body.email
                newUser.username = req.body.username
                newUser.password = hashedPassword

                newUser.save().then(() => {
                    res.status(201).send()
                }).catch(() => {
                    res.status(500).send()
                })
            } catch {
                res.status(500).send()
            }
        }
    });
})

module.exports = router