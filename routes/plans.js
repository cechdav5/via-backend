const express = require('express')
const router = express.Router();
const Plan = require("../models/Plan")
const authorization = require("../accessories/authorization")

function stripUnwantedProperties(plan) {
    const {__v, user, ...ret} = plan
    return ret
}

router.get("/list", authorization, (req, res) => {
    Plan.find({user: req.user._id}).then(saved => {
        strippedSaved = saved.map(plan => stripUnwantedProperties(plan["_doc"]))
        res.json(strippedSaved)
    }).catch(() => {
        res.status(500).send()
    })
})

router.post("/create", authorization, async (req, res) => {
    let newPlan = new Plan()
    newPlan.user = req.user._id
    newPlan.name = req.body.name
    newPlan.description = req.body.description
    newPlan.position = req.body.position
    newPlan.startDate = req.body.startDate
    newPlan.endDate = req.body.endDate
    newPlan.attendees = req.body.attendees

    newPlan.save().then(saved => {
        res.json(stripUnwantedProperties(saved["_doc"]))
    }).catch(() => {
        res.status(500).send()
    })
})

router.delete("/delete", authorization, async (req, res) => {
    Plan.findOneAndDelete({_id: req.query.id, user: req.user._id}).then(doc => {
        if (doc) {
            res.json(stripUnwantedProperties(doc["_doc"]))
        } else {
            res.status(400).send()
        }
    }).catch(err => {
        res.status(500).send()
    })
})

module.exports = router