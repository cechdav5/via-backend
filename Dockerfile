FROM node:18.12.1-alpine

WORKDIR /usr/backend/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 5050
CMD ["npm", "start"]